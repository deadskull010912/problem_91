<?php

/**
 * Fibonacci with recursion
 *
 * @param $n
 *
 * @return mixed
 */
function fibonacci($n) {
    if ($n < 3 ) {
        return $n;
    } else {
        return (fibonacci($n - 1) + fibonacci($n - 2));
    }
}

print 'Fibonacci with recursion';
echo '</br>';
for ($i = 0; $i <= 12; $i++) {
    print fibonacci($i);
    echo '</br>';
}

/**
 * Sum of first 100 items
 *
 * @param $count
 * @param $innerSum
 *
 * @return int
 */
function sumOfNumbers($count, $innerSum) {
    if ($count > 0) {
        $innerSum = $innerSum + $count;
        return sumOfNumbers($count - 1, $innerSum);
    }
    return $innerSum;
}

echo '</br>';
print 'Sum of first 100 items';
echo '</br>';
print sumOfNumbers(100, 0);

echo '</br>';
print 'Problem 91';
echo '</br>';

function problemOnTime($day, $onTime, $absent, $late, $amount, $allDays, $countConsecutiveAbsent, $string) {
    if ($day == $allDays) {
        return $amount + 1;
    }
    if ($day > $allDays) {
        return $amount;
    }

    $amount = problemOnTime($day + 1, $onTime + 1, $absent, $late, $amount, $allDays, 0, $string . 'O');
    $amount = problemAbsent($day + 1, $onTime, $absent + 1, $late, $amount, $allDays, 0, $string . 'A');

    if ($late < 1) {
        $amount = problemLate($day + 1, $onTime, $absent, $late + 1, $amount, $allDays, 0, $string . 'L');
    }

    return $amount;
}

function problemLate($day, $onTime, $absent, $late, $amount, $allDays, $countConsecutiveAbsent, $string) {
    if ($day == $allDays) {
        return $amount + 1;
    }else if ($day > $allDays || $late > 1) {
        return $amount;
    } else if ($late == 1) {
        $amount = problemAbsent($day + 1, $onTime, $absent + 1, $late, $amount, $allDays, 0, $string . 'A');
        $amount = problemOnTime($day + 1, $onTime + 1, $absent, $late, $amount, $allDays, 0, $string . 'O');
    } else if ($late < 1) {
        $amount = problemLate($day + 1, $onTime, $absent, $late + 1, $amount, $allDays, 0, $string . 'L');
    }

    return $amount;
}

function problemAbsent($day, $onTime, $absent, $late, $amount, $allDays, $countConsecutiveAbsent, $string) {
    if ($day == $allDays) {
        return $amount + 1;
    }else if ($day > $allDays || $countConsecutiveAbsent > 3) {
        return $amount;
    } else if ($countConsecutiveAbsent <= 3) {
        $countConsecutiveAbsent++;

        if ($countConsecutiveAbsent < 2) {
            $amount = problemAbsent($day + 1, $onTime, $absent + 1, $late, $amount, $allDays, $countConsecutiveAbsent, $string . 'A');
        }

        if ($late < 1) {
            $amount = problemLate($day + 1, $onTime, $absent, $late + 1, $amount, $allDays, $countConsecutiveAbsent, $string . 'L');
        }

        $amount = problemOnTime($day + 1, $onTime + 1, $absent, $late, $amount, $allDays, $countConsecutiveAbsent, $string . 'O');
    }

    return $amount;
}

echo problemOnTime(0, 0, 0, 0, 0, 30, 0, '');
